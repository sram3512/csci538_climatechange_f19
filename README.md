# csci538_climatechange_f19

VR project of caribou for csci538 - fall 2019 @ USC. The projects objective is to show the effects of climate change on the caribou. This project combines the use of point cloud and mesh structure.

Directory Information:

- cariboulas; Caribou point cloud rendered in VR.
- finalpresentation; Contains caribou with point cloud & mesh. Also an on trigger based narrative is included into it. 

Game Engine - Unreal
